public class Math
{	
	public boolean isPrime(int number) 
	{
		int curDivider = 2;
		boolean isPrime = true;
		while(curDivider<number)
		{
			if(number%curDivider == 0)
			{
				isPrime = false;
				break;
			}
			curDivider++;		
		}
		return isPrime;	
	}
	
	public boolean isEven(int number)
	{
		if( number % 2 == 0)
		{
			return true;
		}		
		else {
			return false;
		}
	}
}