import java.util.Random;

public class Main
{
	public static void main(String[] args)
	{
		Math math = new Math();
		int rdmNumber= new Random().nextInt();
		System.out.println(rdmNumber + 
				" is prime: " + math.isPrime(rdmNumber));
		System.out.println(rdmNumber + 
				" is even: " + math.isEven(rdmNumber));
	}
}

