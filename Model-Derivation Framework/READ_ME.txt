This repository contains the modules of the model-driven framework for timing analysis that is presented in the technical report: http://eprints.eemcs.utwente.nl/26622/. 

The contents of the repository is as follows:
	Example Java Application: folder
		Provides the source code and model of the example Java application presented in the technical report.
			
			Models: folder
			
			Source Code: folder
	Model Transformation Modules: folder
		Provides the transformation modules presented in the technical report.
		
			m2t: folder
				Provides the transformation modules for model-to-text transformation.
			
				uppaal2xsd.etl: file
					This transformation achieves model-to-text transformation for UPPAAL models.				
				
			metamodels: folder
				Provides the metamodels in this framework. Currently, it includes only Graph.ecore. The other metamodels can be downloaded from the references in the technical report.
				
			deleteUnconnectedNodes.etl: file
				This transformation removes the unreachable instructions (from the starting instruction of a method) in a JBCPP model.
				It also has two operations to delete the methods without any instructions and the classes without any methods.
				The abstract methods are removed. 
				Input: JBCPP model
				Output: same input JBCPP model without disconnected nodes.
			
			groupSequences.etl: file
				This transformation groups a sequence of instructions in a JBCPP model into one instruction.
				Input: JBCPP model
				Output: same input JBCPP model with the group instructions added.
						
			jbcpp2uppaal_framework_version.etl: file
				This transformation is corresponds to Action 3 in the paper. This version handles inheritance and polyphormism correclty.
				Note: This transformation assumes that abstract methods are removed.
				The transformation misses the following points:
					- A dummy lazy transformation is provided at the end due to the following bug: https://bugs.eclipse.org/bugs/show_bug.cgi?id=438615.
					- Due to this bug, I had to add hash/maps for keeping track of created locations	
				
			loopAugmentation.etl: file
			loopOperations.eol: file
				This transformation takes a JBCPP model and augments it with the loop augmentation. The transformation uses loopOperations.eol file.
				Input: JBCPP model
				Output: same input JBCPP model with the loop information.
						
			statisticsOfJBCPPModel.etl: file
			statisticsOfUPPAALModel.etl: file
				These two transformations process the related models in a read-only manner to provide statistical information.			
			
			callDepth.etl: file 
					Calculates the call depth of a JBCPP model.
			callGraph.etl: file 
					Input: A JBCPP model
					Output: A call graph conforming to Graph meta-model.
			callTree.etl: file 
					Input: A JBCPP model
					Output: A call graph conforming to Graph meta-model.
		
			detectRecursion.etl: file 
				It takes a JBCPP model as input. It detects recursion.