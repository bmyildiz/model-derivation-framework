/*
@author: Steven, Bugra
	This transformation groups a sequence of instructions in a JBCPP model into one instruction.
	Input: JBCPP model
	Output: same input JBCPP model with the group instructions added.

	The transformation misses the following points:
	- It does not delete the unconnected instructions since it can lead to information loss.	
*/
pre pre_groupSequences
{
	var javaDate = new Native("java.util.Date");
	javaDate.toString().println("M2M GroupSequences transformation is starting: ");
	In.modelFileUri.println("Input model: ");
	In.metamodelUris.println("Input metamodels: ");	
	
	//For giving unique index numbers for groups, the algorithm first calculates the largest index number. 
	// Set groupIndex to the highest index used, so we can assign unique numbers from there onwards
	var groupIndex = 0;
	for (i in Instruction.all) 
	{
		if (i.index > groupIndex) 
		{
			groupIndex = i.index;
		}
	}
	
	var methodHash = prepareMethodHash();
}

post post_groupSequences
{
	javaDate = new Native("java.util.Date");
	javaDate.toString().println("M2M GroupSequences transformation has been finished successfully: ");
}

/*
This is for the first instructions of the methods
*/
@greedy
rule createInitGroup 
	transform instruction: Instruction to groupInstruction: GroupInstruction, edge: UnconditionalEdge 
{
	guard: instruction.isGroupingAllowed() and not instruction.isKindOf(GroupInstruction) and instruction.inEdges.size() = 0 and instruction.parent.firstInstruction = instruction
	
	groupIndex = groupIndex + 1;
	
	instruction.parent.instructions.add(groupInstruction);
	instruction.parent.firstInstruction = groupInstruction;
	
	groupInstruction.firstInGroup = instruction;
	groupInstruction.linenumber = instruction.linenumber;
	groupInstruction.index = groupIndex;
	groupInstruction.minTime = instruction.minTime; // TODO: Add correct time bounds
	groupInstruction.maxTime = instruction.maxTime; // TODO: Add correct time bounds
	groupInstruction.outEdges.add(edge);
	groupInstruction.parent = instruction.parent;
	
	edge.start = groupInstruction;
	edge.end = instruction.getInstructionAfterGroup();
	edge.end.inEdges.add(edge);
	
	// Give group some name for nicer visualization
	groupInstruction.humanReadable = "Group " + instruction.linenumber + "." + instruction.index + "-" + edge.end.linenumber + "." + edge.end.index;
}

@greedy
rule createGroup 
	transform instruction: Instruction to groupInstruction: GroupInstruction, edge: UnconditionalEdge 
{
	guard: instruction.isGroupingAllowed() and not instruction.isKindOf(GroupInstruction) and ((instruction.inEdges.size() = 1 and not instruction.inEdges.get(0).start.isGroupingAllowed()) or instruction.inEdges.size() > 1)
	
	groupIndex = groupIndex + 1;
	
	instruction.parent.instructions.add(groupInstruction);
	
	for (inEdge in instruction.inEdges.clone) 
	{
		inEdge.end.inEdges.remove(inEdge);
		inEdge.end = groupInstruction;
		groupInstruction.inEdges.add(inEdge);
	}
	
	groupInstruction.firstInGroup = instruction;
	groupInstruction.linenumber = instruction.linenumber;
	groupInstruction.index = groupIndex;
	groupInstruction.minTime = instruction.minTime; // TODO: Add correct time bounds
	groupInstruction.maxTime = instruction.maxTime; // TODO: Add correct time bounds
	groupInstruction.outEdges.add(edge);
	groupInstruction.parent = instruction.parent;
	
	edge.start = groupInstruction;
	edge.end = instruction.getInstructionAfterGroup();
	edge.end.inEdges.add(edge);
	
	// Give group some name for nicer visualization
	groupInstruction.humanReadable = "Group " + instruction.linenumber + "." + instruction.index + "-" + edge.end.linenumber + "." + edge.end.index;
}

/*
The instruction after group can only be a method call (whose method is included), a decision point or a meeting/union point 
*/
operation Instruction getInstructionAfterGroup(): Instruction 
{
	if (not self.isGroupingAllowed() or self.inEdges.size() > 1) 
	{
		return self;
	} 
	else 
	{
		return self.outEdges.get(0).end.getInstructionAfterGroup();
	}
}

/*
This operation is for checking following conditions:
- If the instruction is NOT a method call instruction whose method is included in the model.
- If the instruction is NOT a decision point
*/
operation Instruction isGroupingAllowed(): Boolean 
{
	return (not self.isKindOf(MethodInstruction) or ( self.isKindOf(MethodInstruction) and self.getMethod() = null )) and self.outEdges.size() = 1;
}

//Returns a hashmap of all Method instances in the input model. Key is name + desc + ownerClassName
operation prepareMethodHash():Map
{
	var methodHash = new Map();
	var allMethodInstances = In!Method.allInstances();
	for(m:In!Method in allMethodInstances)
	{
		methodHash.put( m.name +  m.descriptor + m.parent.name , m);
	}
	return methodHash;	
}

//Returns the method instance for this MethodInstruction
//Note: If the called method is not included in the model, then this operation returns null.
operation In!MethodInstruction getMethod(): In!Method
{
	return methodHash.get(self.name + self.desc + self.owner);	
}