/*
@author: Bugra
	This transformation calculates and outputs the deepest call chain in a call graph conforming to Graph metamodel. It does not modify the input model.
	Input: A call graph conforming to Graph metamodel.
	Output: The same with the input model.

	Note 1: This transformation assumes that there is only one main/starting/root node. In toher words, there is only one node in the graph with no incoming edges. 

@TODO: This tag shows the parts which requires revising later.
*/

pre pre_callDepth
{
	"M2M callDepth transformation is starting.".println();
	
	//Finding the main node
	var startNode = null;
	
	for(curNode:In!Node in In!Node.allInstances())
	{
		if(curNode.incoming.size() = 0)
		{
			startNode = curNode;
		}
	}
	
	startNode.getDeepestCallChain(new OrderedSet()).invert().println("Call depth: ");	
}

post post_callDepth
{
	"M2M callDepth transformation has been finished successfully.".println("\n\n");
}

//Returns the deepest call chain of the given node in a OrderedSet
operation In!Node getDeepestCallChain(visitedNodes: OrderedSet): OrderedSet
{
	self.println("Call getDeepestCallChain: ");
		
	//Base case: If there is no outgoing edges
	if(self.outgoing.size() = 0)
	{
		var callStack = new OrderedSet();
		callStack.add(self);
		self.println("End getDeepestCallChain: ");
		return callStack;		
	}
		
	//Recursive part: It returns the longest list from all the outgoing edges
	var curDeepestCallStack = new OrderedSet();
	
	//For each outgoing edge
	for(curEdge:In!Edge in self.outgoing)
	{
		if(not visitedNodes.includes(curEdge.target))//Do not visit the node which is already visited
		{
			var updatedVisitedNodes = new OrderedSet();
			updatedVisitedNodes.add(self);
			updatedVisitedNodes.add(visitedNodes);
			
			var curCallStack = curEdge.target.getDeepestCallChain(updatedVisitedNodes);
			
			if(curCallStack.size() > curDeepestCallStack.size())
			{
				curDeepestCallStack = curCallStack;
			}
		}
	}
	
	if(curDeepestCallStack.size() > 0)//If there are some other calls which do not create recursion
	{
		curDeepestCallStack.add(self);
		self.println("End getDeepestCallChain: ");
		return curDeepestCallStack;
	}
	else//There are only recursive creating calls
	{
		self.println("End getDeepestCallChain: ");
		return new OrderedSet();
	}
}