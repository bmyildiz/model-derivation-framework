/*
@author: Bugra
	This transformation derives a call tree of a JBCPP model. It does not go further where the recursion starts.
	Input: A JBCPP model
	Output: A call graph conforming to Graph meta-model.

	Note 1: This transformation assumes that the method which does not have any callers is the main method. In other words, the transformation assumes there are
no uncalled methods in the input model.
	Note 2: No explicit support for inheritance.
	Note 3: The methods which are not included in the model are not considered as method calls

@TODO: This tag shows the parts which requires revising later.
*/

pre pre_callTree
{
	"M2M callTree transformation is starting.".println();
	
		
}

//Creates the root nodes of the output model and executes the call tree derivation algorithm
rule jbcpp_Project2callTreeRoot
	transform in_project:In!Project to out_graph:Out!Graph
{
	//Adding the main method
	var mainMethod = getMainMethod();	
	
	//@TODO: Verification check for main method
	mainMethod.println("Main method is: ");
	if(not in_project.mainClass.methods.includes(mainMethod))
	{
		"Error: The detected main method does not belong to the main class.".println();
	}
	
	//Initialization
	var methodQueue = new OrderedSet();
	var nodeQueue = new OrderedSet();
	
	var newNode = new Out!Node;
	newNode.name = mainMethod.getNodeName();
	out_graph.nodes.add(newNode);
	
	
	//Adding the initial members to the queues
	methodQueue.add(mainMethod);
	nodeQueue.add(newNode);
	
	//While there are still methods to process
	while(methodQueue.size <> 0)
	{
		//Enqueue the elements
		var curMethod = methodQueue.removeAt(0);
		var curNode = nodeQueue.removeAt(0);
		
		//For each called method in this method
		for(mi:In!MethodInstruction in curMethod.instructions)
		{
			var curMethodOfInstruction = mi.getMethod();
			
			if(curMethodOfInstruction <> null and not mi.isRecursion())
			{		
				//Find the method itself and add to the queue
				methodQueue.add(curMethodOfInstruction);
				
				//Create a node in the call graph for this method call and create edge
				var newNode = new Out!Node;
				newNode.name = curMethodOfInstruction.getNodeName();
				nodeQueue.add(newNode);
				out_graph.nodes.add(newNode);
				
				var newEdge = new Out!Edge;
				newEdge.source = curNode;
				newEdge.target = newNode;			 
			}
		}
	}
}

//@TODO This rule should be removed after the bug fix.
@lazy 
rule bla transform in_project:In!Project to null
{
	"bla: This rule shoud be removed after the bug fix.".println();
}

post post_callTree
{
	"M2M callTree transformation has been finished successfully.".println("\n\n");
}


//Returns the node name of the method
@cached
operation In!Method getNodeName(): String
{
	var className = self.getOwnerClass().name;
	var methodName = self.name;
	var methodDescription = self.descriptor;

	return className + "::" + methodName + "_" + methodDescription; 
}

//Returns the main method
operation getMainMethod(): In!Method
{
	var allMethodInstances = In!Method.allInstances();
	
	//Initializing all methods as not called
	for(m:In!Method in allMethodInstances)
	{
		m.~isCalled = false;		
	}
	
	//Mark the methods which are called
	var allMethodInstructions = In!MethodInstruction.allInstances();
	for(mi:In!MethodInstruction in allMethodInstructions)
	{
		var calledMethod = mi.getMethod();
		if(calledMethod <> null)
		{
			calledMethod.~isCalled = true;
		}
	}
	
	//Return the method which is not called
	for(m:In!Method in allMethodInstances)
	{
		if(not m.~isCalled)
		{
			return m;
		}		
	}	
	
	"Problem in callTree:getMainMethod()  : No main method has been found".println();
	return null;
}

//Returns true if this method instruction creates recursion 
//@TODO: Detects only direct recursion
@cached
operation In!MethodInstruction isRecursion(): Boolean
{
	if(self.getMethod() = self.getOwnerMethod())
	{
		return true;
	}
	
	return false;
}

//Returns the method instance for this MethodInstruction
//Note: If the called method is not included in the model, then this operation returns null.
@cached
operation In!MethodInstruction getMethod(): In!Method
{
	var allMethodInstances = In!Method.allInstances();
	for(m:In!Method in allMethodInstances)
	{
		if(self.name = m.name and self.desc = m.descriptor and self.owner = m.getOwnerClass().name)
		{
			return m;
		}
	}
	
	//"Method is not found.".errln("operation In!MethodInstruction getMethod(): In!Method ::");
	return null;
}

//Returns the owner In!Method for this In!Instruction
@cached
operation In!Instruction getOwnerMethod(): In!Method
{
	var methodList = In!Method.allInstances();
	for(m: Method in methodList)
	{
		if(m.instructions.includes(self))
		{
			return m;
		}
	}
	
	"Owner method is not found.".errln("operation In!Instruction getOwnerMethod(): In!Method ::");
	return null;
}

//Returns the owner class of the given instruction
operation In!Instruction getOwnerClass(): In!Clazz
{
	var methodList = In!Method.allInstances();
	for(m: Method in methodList)
	{
		if(m.instructions.includes(self))
		{
			return m.getOwnerClass();
		}
	}
	
	"Owner class is not found.".errln("operation In!Instruction getOwnerClass(): In!Clazz ::");
	return null;
}

//Returns the owner class of the given method
operation In!Method getOwnerClass(): In!Clazz
{
	//Iterate on all the class instances
	var classList = In!Clazz.allInstances();
	for (c: In!Clazz in classList)
	{
		if(c.methods.includes(self))
		{
			return c;
		}
	}		
	
	"Owner class is not found.".errln("operation In!Method getOwnerClass(): In!Clazz ::");
	return null;
}